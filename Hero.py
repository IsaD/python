class SuperHero:
    people = 'people'

    def __init__(self, name, nickname, superpower, health_points, catchphrase):
        self.name = name
        self.nickname = nickname
        self.superpower = superpower
        self.health_points = health_points
        self.catchphrase = catchphrase

    def display_name(self):
        print(f"name: {self.name}")

    def double_health_points(self):
        self.health_points *= 2

    def __str__(self):
        return f"nickname: {self.nickname}\n" \
               f"superpower: {self.superpower}\n" \
               f"health: {self.health_points}"

    def __len__(self):
        return len(self.catchphrase)

Hero1 = SuperHero('ironman', 'jelezo', 'nothing', 100, 'Im playboy, genius, philanthrop')

Hero1.display_name()
Hero1.double_health_points()
print(Hero1)
print(len(Hero1.catchphrase))

class AirHero(SuperHero):
    def __init__(self, name, nickname, superpower, health_points, catchphrase, damage):
        super().__init__(name, nickname, superpower, health_points, catchphrase)
        self.damage = damage
        self.fly = False


class EarthHero(SuperHero):
    def __init__(self, name, nickname, superpower, health_points, catchphrase, damage):
        super().__init__(name, nickname, superpower, health_points, catchphrase)
        self.damage = damage
        self.fly = False

class Villain(EarthHero):
    people = 'monster'

    def gen_x(self):
        pass

    def crit(self, damage):
        self.damage **= 2


air_hero = AirHero('Captain Marvel', 'Marvel', 'Overpowered', 100, 'I"m a Captain Marvel', 99)
air_hero.double_health_points()
air_hero.fly_in_the_phrase('sky')
print(f"Health Points: {air_hero.health_points}")
print(f"Fly: {air_hero.fly}")

earth_hero = EarthHero('Earthman', 'Earthy', 'Earth manipulation', 150, 'Feel the power of the earth', 100)
earth_hero.double_health_points()
earth_hero.fly_in_the_phrase('ground')
print(f"Health Points: {earth_hero.health_points}")
print(f"Fly: {earth_hero.fly}")

villain = Villain('Thanos', 'thanos', 'big man', 300, 'Velikiy Black man', 101)
villain.double_health_points()
villain.fly_in_the_phrase('shadows')
villain.gen_x()
villain.crit(200)
print(f"Health Points: {villain.health_points}")
print(f"Fly: {villain.fly}")
print(f"Damage: {villain.damage}")