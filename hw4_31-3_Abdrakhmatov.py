class AG:
    def __init__(self, age):
        self.__age = age

class NA:
    def __init__(self, name):
        self.name = name

class N:
    def Method1(self):
        pass

class NN:
    def Method2(self):
        pass

class SuperNN(AG, NA, N, NN):
    def __init__(self, name, age):
        super().__init__(age)  # Исправлено: super().__init__(age)
        self.name = name  # Исправлено: self.name = name

    @property
    def age(self):
        return self.__age

    @age.setter
    def age(self, new_age):
        self.__age = new_age
